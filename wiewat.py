
import numpy as np
import random

# returnt een lijst met alle mogelijke vragen.
def onderzoeksgroepen():
    groepen = ['Theory of Computer Science', 'Computational Science Lab',
               'Parallel Computing Systems', 'System and Network Engineering Lab',
               'Intelligent Sensory Information Systems',
               'Centrum voor Wiskunde en informatica']
    return groepen

def vragen():
    ans = {
        'Theory of Computer Science' : {
        'Maakt gebruik van algoritmes' : [True, 'Kom op het maakt er gebruik van']

        },
        'Computational Science Lab' : {

        },
        'Parallel Computing Systems' : {

        },
        'System and Network Engineering Lab' : {

        },
        'Intelligent Sensory Information Systems' : {

        }
        'Centrum voor Wiskunde en informatica' : {

        }
    }
    return ans

# Legt de regels uit.
def introductie():
    print('Welkom bij Wie is deze onderzoeksgroep, de nieuwe en uiterst ', \
          'populaire variant van Wie is het.')
    print('De bedoeling is om met zo min mogelijk vragen op de juiste ', \
          'onderzoeksgroep uit te komen. Je mag elke ronde raden als je denkt', \
          ' te weten wat het correcte antwoord is. Maar eenmaal fout geraden is', \
          'game over.')
    print('Elke ronde krijg je de keuze uit een aantal vragen om te stellen, na ', \
          'de vraag te hebben gestelt hoor je of dit waar of niet waar is. ', \
          'Als de vraag klopt worden de antwoorden die hier niet bij horen ', \
          'weggestreept. Wanneer het antwoord niet klopt moet je zelf bepalen '\
          'welke antwoorden je wilt wegstrepen als je uberhaubt iets wilt wegstrepen.' \
          'Als je de goede onderzoeksgroep wegstreept ben je game over.')


# Vraagt aan speler wanneer hij klaar is om te beginnen.
def klaar():
    while True:
        try:
            if input("Typ 'r' om te beginnen: ") != 'r':
                print('dat is niet de letter r.')
            else:
                break
        except ValueError:
            print("try again.")

def main():
    groepen = onderzoeksgroepen()
    dict_vragen = vragen()
    introductie()
    klaar()
    # dit is de gekozen onderzoeksgroep waar naartoe wordt gewerkt
    antwoord = groepen[random.randint(0,5)]
    while True:
        # moet nog 8 random vragen gekozen worden.
        # manier om die vragen te kiezen
        # choice is nummer van de vraag.
        while True:
            try:
                choice = input('Typ nummer van gekozen vraag.')
                if choice in [1..8]:
                    break
                else:
                    continue
            except ValueError:
                print("try again.")

        check, ans = dict_vragen[antwoord][vraag]

        if check == True:
            print(ans)
            # moet wegstrepen welke niet meer bij horen.
        else:
            print(ans)
            # manier om onderzoeksgroepen weg te strepen.
            # wanneer goede antwoord wordt weggestreept, game over.


if __name__ == "__main__":
    main()
